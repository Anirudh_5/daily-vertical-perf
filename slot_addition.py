import random

slots = ["BREAKFAST","LUNCH","DINNER"]

with open('lat-long.txt','r') as file:
	lines = file.read().splitlines()

with open('lat-long-slot.txt','w') as f:
	for line in lines:
		slot = random.choice(slots)
		print(line+","+slot, file=f)