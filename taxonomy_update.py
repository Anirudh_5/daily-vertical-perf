import json
import requests
import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

create_taxonomy_url = "http://swiggy-taxonomy-service.sf.swiggyops.de/taxonomy/store/{}"
publish_taxonomy_url = "http://swiggy-taxonomy-service.sf.swiggyops.de/taxonomy/store/{}/{}/publish"
taxonomy_json = {
        "store_id": "220917",
        "display_name": "220917 Daily Taxonomy",
        "taxonomy_type": "All Listing",
        "enabled": True,
        "nodes": [
            {
                "enabled": False,
                "node_type": "2",
                "rule_sets": [],
                "nodes": [
                    {
                        "enabled": False,
                        "node_type": "1",
                        "rule_sets": [
                            {
                                "rules": [
                                    {
                                        "enabled": False,
                                        "key": "category",
                                        "operator": "EQ",
                                        "value": "3a525919-94aa-4417-8c6d-a4d9f891023b",
                                        "tags": []
                                    }
                                ],
                                "meta": {},
                                "enabled": False
                            }
                        ],
                        "nodes": [],
                        "images": [],
                        "tags": [],
                        "meta": {},
                        "node_name": "Plans"
                    },
                    {
                        "enabled": False,
                        "node_type": "1",
                        "rule_sets": [
                            {
                                "rules": [
                                    {
                                        "enabled": False,
                                        "key": "category",
                                        "operator": "EQ",
                                        "value": "d0346f81-7470-446a-ace8-961ef06aeb72",
                                        "tags": []
                                    }
                                ],
                                "meta": {},
                                "enabled": False
                            }
                        ],
                        "nodes": [],
                        "images": [],
                        "tags": [],
                        "meta": {},
                        "node_name": "Meals"
                    },
                    {
                        "enabled": False,
                        "node_type": "1",
                        "rule_sets": [
                            {
                                "rules": [
                                    {
                                        "enabled": False,
                                        "key": "category",
                                        "operator": "EQ",
                                        "value": "1a244535-7c81-4521-9417-cbf4c8bc8a7a",
                                        "tags": []
                                    }
                                ],
                                "meta": {},
                                "enabled": False
                            }
                        ],
                        "nodes": [],
                        "images": [],
                        "tags": [],
                        "meta": {},
                        "node_name": "Addons"
                    }
                ],
                "images": [],
                "tags": [],
                "meta": {},
                "node_name": "Root"
            }
        ],
        "tags": [],
        "meta": {},
        "priority": 0,
        "images": []
}

#check from consul
mySQLconnection = mysql.connector.connect(host='hostname',
                                            database='swiggy',
                                             user='username',
                                         password='password')

try:
    cursor = mySQLconnection.cursor()
    db_update_query = "UPDATE `restaurants` SET `enabled`=1 WHERE `id`='{}'"

    headers = {'Content-Type': 'application/json'}
    with open("restaurant_ids.txt", mode="r") as file:
        res = file.read()
        trim_res = res.replace('\"','')
        print(trim_res)
        store_ids = trim_res.split(',')
        for i in range(len(store_ids)-1):
            store_id = store_ids[i]
            print(store_id)
            #db update
            update_string = db_update_query.format(store_id)
            print(update_string)
            cursor.execute(update_string)
            mySQLconnection.commit()
            ##--create taxonomy--##
            url = create_taxonomy_url.format(store_id)
            print("URL: "+url)
            taxonomy_json["store_id"] = store_id
            taxonomy_json["display_name"] = store_id+" Daily Taxonomy"
            #print(taxonomy_json)
            response = requests.post(url, json.dumps(taxonomy_json), headers=headers)
            response_json = response.json()
            print("taxonomy id: "+response_json["taxonomy_id"])
            ##--publish taxonomy--##
            taxonomy_id = response_json["taxonomy_id"]
            publish_url = publish_taxonomy_url.format(store_id,taxonomy_id)
            response_publish = requests.put(publish_url, headers=headers)
            if response_publish.ok:
                print("successful")
            else: 
                print("error")

        cursor.close()

except Error as error:
    print("Failed to insert record into Laptop table {}".format(error))

finally:
        if (mySQLconnection.is_connected()):
                mySQLconnection.close()
                print("MySQL connection is closed")