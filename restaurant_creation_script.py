import random
import csv
import json
import requests
import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

rest_creation_url = "http://swiggy-restaurant-services.sf.swiggyops.de/restaurant/"
marking_daily_url = "http://swiggy-restaurant-services.sf.swiggyops.de/restaurant/metadata"
vendor_story_update_url = "http://swiggy-restaurant-services.sf.swiggyops.de/restaurant/story/uploadBranding"
delivery_serviceable_url = "http://solr-listing.u4.swiggyops.de/solr/core_listing/update/json/docs?commit=true"
vendor_story_data = {
	"rest_id": 74277,
	"template_data": {
		"title": "Bon Appetit Restaurant",
		"description": "Alpha Beta Gamma",
		"images": [
			"ivb7890",
			"tyui890"
		],
		"quotes": {
			"type": 1,
			"description": "Alpha Beta Gamma Delta",
			"vendorName": "Palak Maheswari",
			"vendorDesignation": "Owner"
		}
	}
}
rest_serviceable_data = {
		"id":"221112",
		"enabled":True,
		"name":"Tandoor Hut",
		"place":"28.49905335351815,77.08621066063643",
		"area_id":30,
		"city_id":2,
		"is_long_distance_enabled":True,
		"type":"F",
		"partner_id":2,
		"zone_id":1,
		"cost_for_two":410.609,
		"commission_exp":0.0,
		"commission":0.0,
		"model_based_rating":"4.04",
		"is_daily_restaurant": True
}
marking_daily_data = {
	"restaurantId" : 221100,
	"attribute" :"is_daily_restaurant",
	"value" : 1
}
rest_creation_data = {
	"name": "daily_perf_test",
	"address": "test",
	"locality": "DLF Phase 5",
	"area": "30",
	"city": "2",
	"mou": "test",
	"latitude": "28.49905335351815",
	"longitude": "77.08621066063643",
	"gst": {
		"state": "IN-UP",
		"gstin": "33FBFTM5865F177",
		"isGstPresent": False,
		"kycDocUrl": "https://url.com/kyc/doc.pdf",
		"gstDocUrl": "https://restaurant-mou.s3.amazonaws.com/gst-doc/gstf-doc-1.pdf",
		"businessName": "TN Company 968 Ltd",
		"stateJurisdiction": "ADYAR",
		"taxPayerType": "REGULAR_DEALER",
		"registeredDate": "2019-03-30",
		"constitutionOfBusiness": "SOLE_PROPRIETORSHIP",
		"centerJurisdiction": "ALLAHABAD-I",
		"itemSgst": "0.025",
		"itemCgst": "0.025",
		"itemIgst": "0.025",
		"itemInclusive": "true",
		"packagingChargeSgst": "0",
		"packagingChargeCgst": "0",
		"packagingChargeIgst": "0",
		"packagingChargeInclusive": "true"
	},
	"fssai": {
		"isFssaiPresent": "true",
		"fssaiDocUrl": "https://url.com/fssai/doc.pdf",
		"registeredName": "asdfasdf",
		"addressPremises": "asdfasdf",
		"postalCode": "581630",
		"licenseNumber": "12312312312312",
		"expiryDate": "2020-13-60",
		"referenceNumber": "123f",
		"applicationDate": "2019-13-60"
	},
	"mouTypeDigital": True,
	"vymoId": "test0101",
	"cancelledCheque": "testV",
	"compId": "test",
	"agreementType": "post_paid",
	"updatedBy": "test",
	"bankCity": "Mumbai",
	"ownerEmail": "test@gmail.com",
	"ownerMobile": "12345",
	"commissionExp": "test",
	"payBySystemValue": "1",
	"gstState": "test",
	"invoicingName": "test",
	"bankName": "test",
	"bankIfscCode": "test",
	"bankAccountNo": "test",
	"tanNo": "1234",
	"panNo": "1234",
	"benificiaryName": "123",
	"invoicingEmail": "amareshwar.mb@swiggy.in",
	"ownerName": "qwerty",
	"smEmail": "qwe@swiggy.in",
	"asmEmail": "erty@swiggy.in",
	"costForTwo": "200",
	"isVeg": "0",
	"orderNotifyEmail": " asdf@asdf.coa , asdf@asdf.com ",
	"orderNotifyNumber": " 9494949494 , 9494949494  ",
	"prepTimeNormal": "15",
	"prepTimePeak": "20",
	"packagingCharge": "20",
	"restaurantSlots": [
		{
			"day": "Mon",
			"open": "0000",
			"close": "2360"
		},
		{
			"day": "Tue",
			"open": "0000",
			"close": "2360"
		},
		{
			"day": "Wed",
			"open": "0000",
			"close": "2360"
		},
		{
			"day": "Thu",
			"open": "0000",
			"close": "2360"
		},
		{
			"day": "Fri",
			"open": "0000",
			"close": "2360"
		},
		{
			"day": "Sat",
			"open": "0000",
			"close": "2360"
		},
		{
			"day": "Sun",
			"open": "0000",
			"close": "2360"
		}
	],
	"creationSource": "2"
}

#check from consul
mySQLconnection = mysql.connector.connect(host='hostname',
														 database='swiggy',
														 user='username',
														 password='password')


try:
	cursor = mySQLconnection.cursor()
	db_insert_query = """INSERT INTO `restaurant` (`id`, `name`, `area_code`, `type`, `with_de`, `lat_long`, `enabled`, `archived`, `batching_enabled`, `third_party_delivery_enabled`, `prep_time_peak`, `max_banner_factor`, `max_second_mile`, `rain_mode`, `jit_enabled`, `address_verified`, `partner_id`, `self_delivery_override`, `fixed_sla`, `serviceable`, `max_active_orders`, `vendor_rain_mode`, `polygon_id`, `max_long_distance_last_mile`, `trust_rest_prep_time`, `o2p_buffer`, `jit_enabled_soft_tagging`, `is_long_distance_enabled`, `batching_version`, `is_daily_restaurant`, `colocated_restaurant_group_id`, `airport_model_enabled`)
VALUES
	({}, '{}', 30, 'F', 0, '{}', 1, 0, 1, 0, 28, 2.00, 20.00, 1, 0, 1, 23, 0, NULL, 1, NULL, 1, -1, 10, 0, 5.46, 0, 1, 2, 1, NULL, 0);"""

	latitudes = ["28.415087","28.480057","28.496055","28.4653127","28.459933","28.438081","28.490554","28.450947","28.49905335351815","28.460446","28.485116","28.48512407963497","28.47347331682524","28.532181","28.534927","28.5173234","28.517075","28.51161265678195","28.51180472609362","28.511581","28.50084930475792"]
	longitudes = ["77.09288649999999","77.099991","76.984634","77.07701899999999","77.095398","77.08621066063643","77.04073"]
	data_points = []
	headers = {'Content-Type': 'application/json'}
	headers_for_story = {'Content-Type': 'application/json', 'user_meta': '{"source": "vendor"}'}

	#print(len(latitudes))
	#print(len(longitudes))


	for i in range(1,2):
		#restaurant creation
		rest_creation_data["name"] = "daily_test_restaurant" + str(i)
		latitude = random.choice(latitudes)
		longitude = random.choice(longitudes)
		lat_lng = latitude+","+longitude
		rest_creation_data["latitude"] = latitude
		rest_creation_data["longitude"] = longitude
		#print(lat_lng)
		print(rest_creation_data["name"])
		response = requests.post(rest_creation_url, json.dumps(rest_creation_data), headers=headers)
		response_json = response.json()
		print(response_json)
		#restaurant activation as daily
		marking_daily_data["restaurantId"] = response_json["data"]
		print(marking_daily_data)
		activation_response = requests.post(marking_daily_url, json.dumps(marking_daily_data), headers=headers)
		activation_response_json = activation_response.json()
		print(activation_response_json)
		#vendor story
		vendor_story_data["rest_id"] = response_json["data"]
		vendor_story_data["template_data"]["title"] = rest_creation_data["name"]
		story_creation_response = requests.post(vendor_story_update_url, json.dumps(vendor_story_data), headers=headers_for_story)
		story_response_json = story_creation_response.json()
		print(story_response_json)
		#restaurant serviceable
		rest_serviceable_data["id"] = response_json["data"]
		rest_serviceable_data["name"] = rest_creation_data["name"]
		rest_serviceable_data["place"] = lat_lng
		deliveryResponse = requests.post(delivery_serviceable_url, json.dumps(rest_serviceable_data), headers=headers)
		delivery_response_json = deliveryResponse.json()
		print(delivery_response_json)
		#Rest delivery db update
		insert_string = db_insert_query.format(response_json["data"],rest_creation_data["name"],lat_lng)
		cursor.execute(insert_string)
		mySQLconnection.commit()
		print(insert_string)
		#data append in a file
		data_points.append(response_json['data'])
	
	cursor.close()

	with open("restaurant_ids.txt", mode="w") as file:
		for data_point in data_points:
			line = "\"{}\",".format(data_point)
			file.write(line)

except Error as error:
	print("Failed to insert record into Laptop table {}".format(error))

finally:
		if (mySQLconnection.is_connected()):
				mySQLconnection.close()
				print("MySQL connection is closed")
